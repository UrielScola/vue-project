import Home from './components/Home.vue';
import Skills from './components/SkillList.vue';

export const routes = [
  { path: '', component: Home },
  { path: '/skill-list', component: Skills }
];
